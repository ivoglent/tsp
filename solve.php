<?php
/**
 * @created by Long Nguyen <ivoglent@gmail.com>
 * @date : 3rd Jun, 2018
 *
 * @description
 * This script just made for technical test of Zinio Viet Nam
 *
 */


/**
 * Class City
 *
 */
class City
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var double
     */
    public $lat;
    /**
     * @var double
     */
    public $long;

    /**
     * City constructor.
     * Setup city information based on Name, Lat and Long
     *
     * @param $name
     * @param $lat
     * @param $long
     * @throws Exception
     */
    public function __construct($name, $lat, $long)
    {
        $this->name = $name;
        $this->lat = floatval($lat);
        $this->long = floatval($long);
        if (empty($name) || empty($lat) || empty($long)) {
            throw new Exception("Invalid input city {$name} - {$lat} - {$long}");
        }
    }

    /**
     * Get distance from this city to provided city
     * Distance unit is in km
     *
     * @param City $city
     * @return double
     */
    public function getDistance($city)
    {
        $R = 6371;
        $dLat = $this->converDegToRand($city->lat - $this->lat);
        $dLon = $this->converDegToRand($city->long - $this->long);
        $a = sin($dLat / 2) * sin($dLat / 2) + cos($this->converDegToRand($this->lat)) * cos($this->converDegToRand($city->lat)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $d = $R * $c;
        return $d;
    }

    /**
     * @param $deg
     * @return float
     */
    private function converDegToRand($deg)
    {
        return $deg * (M_PI / 180);
    }

}

/**
 * Class TravelMan
 *
 */
class TravelMan
{

    /**
     *The list of cities
     *
     * @var  City[]
     */
    private $cities;

    /** @var array */
    private $visited = [];

    /** @var int */
    public $min = PHP_INT_MAX;

    /** @var int */
    private $minDist = PHP_INT_MAX;

    private $tmp = [];

    /**
     * Total distances of current routes
     *
     * @var int */
    private $costs = 0;

    /**
     * Store good ways
     *
     * @var array
     */
    public $routes = [];

    /**
     * Begin city index
     *
     * @var int
     */
    public $startAt = 1;

    /**
     * Matrix of cities distances
     *
     * @var array
     */
    private $citiesMarix = [];


    /**
     * TravelMan constructor.
     *
     * @param City[] $cities
     */
    public function __construct($cities = [])
    {
        $this->cities = $cities;
    }

    /**
     * @param City[] $cities
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }

    /**
     * Walking through the list of cities
     * @return array
     */
    public function walk()
    {
        //Mark all cities have not visited yet
        for ($i = 1; $i <= count($this->cities); $i++) {
            $this->visited[$i] = false;
        }

        //Fill the matrix of distances
        for ($i = 1; $i <= count($this->cities); $i++) {
            $this->citiesMarix[$i] = [];
            for($j = 1; $j <= count($this->cities); $j++) {
                $dist = $this->cities[$i -1]->getDistance($this->cities[$j-1]);
                $this->citiesMarix[$i][$j] = $dist;
                if ($dist !== 0 && $dist < $this->minDist) {
                    $this->minDist = $dist;
                }
            }
        }

        //The first city is Beijing [at 0 position]
        $this->routes[] = $this->cities[$this->startAt -1]->name;
        //Start cost is zero
        $this->costs = 0;
        $this->visited[$this->startAt] = true;
        $this->tmp[1] = $this->startAt;


        //Try to find the best routes
        $this->getRoutes();

        //Completed return list of cities
        return $this->routes;
    }

    public $c = 0;

    /**
     * Try to find the best route to all of cities
     * @param int $i
     *
     * I know this is not the best solution...
     * currently it still taking to much of time with large of number of cities.
     *
     */
    private function getRoutes($i = 2)
    {
        //Try to find the way from second city
        for ($j = 2; $j <= count($this->cities); $j++) {
            //$city = $this->cities[$j];
            //If not visited yet
            if (!$this->visited[$j]) {
                //Test with this city
                $this->tmp[$i] = $j;
                //Visited this city
                $this->visited[$j] = true;
                //Calculate current distance from previous city
                $this->costs += $this->citiesMarix[$this->tmp[$i -1]][$this->tmp[$i]];//$this->cities[$i - 1]->getDistance($city);
                if ($i === count($this->cities)) {
                    //The distance to come back begin city
                    $back = $this->citiesMarix[$this->tmp[count($this->cities)]][$this->startAt];
                    if ($this->costs + $back < $this->min) {
                        $this->routes = [];
                        for ($k = 1; $k <= count($this->tmp); $k++) {
                            $this->routes[] = $this->cities[$this->tmp[$k] -1]->name;
                        }
                        //Reset the min total of distances
                        $this->min = $this->costs + $back;
                    }
                } else {
                    //try with next city if estimated remaining distance is less than current min distance
                    $estimateCost = $this->costs + (count($this->cities) - $i + 1) * $this->minDist;
                    if ($estimateCost < $this->min) {
                        $this->getRoutes($i + 1);
                    }
                }


                //this is not good way
                $this->visited[$j] = false;
                //Revert total distances
                $this->costs -= $this->citiesMarix[$this->tmp[$i - 1]][$this->tmp[$i]];
            }
        }
    }
}


/** ==========================START========================== */
//et_time_limit(15 * 60); //15 min! :(


/**
 * Create a rich man
 */
$travelMan = new TravelMan();

/**
 * Get the list of cities
 */
$file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'cities.txt';
if (!file_exists($file)) {
    while (true) {
        echo "Please input cities file: (Enter to use default $file)" . PHP_EOL;
        $in = fopen('php://stdin', 'rw+');
        $_file = trim(fgets($in));
        if (!empty($_file)) {
            $file = $_file;
        }
        if (file_exists($file)) {
            break;
        }
    }
}

$cities = [];
//Read the list from file
try {
    $f = fopen($file, 'r');
    while (!feof($f)) {
        $line = trim(fgets($f));
        $ps = preg_split("/\t/", $line);
        if (!isset($ps[0]) || !isset($ps[1]) || !isset($ps[2])) {
            die("Invalid input file format : {$line}" . PHP_EOL);
        }
        $city = new City($ps[0], $ps[1], $ps[2]);
        $cities[] = $city;
    }
} catch (\Exception $e) {
    die('Invalid input file format!');
}

//Give the cities list to travel man
$travelMan->setCities($cities);


//Walk around the world
$result = $travelMan->walk();
foreach ($result as $cityName) {
    echo $cityName . PHP_EOL;
}
echo 'Total : ' . $travelMan->min . ' km' . PHP_EOL;

//End!